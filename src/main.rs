use chrono::{Datelike, DateTime, Duration, Timelike, TimeZone, Utc, Weekday};
use chrono_tz::Tz;
use serenity::{prelude::*, model::prelude::*};
use serde::{Serialize, Deserialize, de::Deserializer, ser::Serializer};
use std::result::Result; // Shadow serenity's `Result`

const TZ: &'static Tz = &chrono_tz::Europe::Paris;

fn next(day: Weekday) -> DateTime<Tz> {
    let mut d = Utc::now().with_timezone(TZ);
    loop {
        d = d + Duration::days(1);
        if d.weekday() == day {
            return d
        }
    }
}

fn make_calendar<'de, D>(d: D) -> Result<(String, web_ical::Calendar), D::Error> where D: Deserializer<'de> {
    let url = String::deserialize(d)?;
    Ok((url.clone(), web_ical::Calendar::new(&url)))
}

fn serde_rwlock<'de, D, T>(d: D) -> Result<RwLock<T>, D::Error> where D: Deserializer<'de>, T: Deserialize<'de> {
    Ok(RwLock::new(T::deserialize(d)?))
}

fn ser_calendar<S>((url, _): &(String, web_ical::Calendar), s: S) -> Result<S::Ok, S::Error> where S: Serializer {
    url.serialize(s)
}

fn ser_rwlock<S, T>(lock: &RwLock<T>, s: S) -> Result<S::Ok, S::Error> where S: Serializer, T: Serialize {
    futures::executor::block_on(lock.read()).serialize(s)
}

#[derive(Clone, Serialize, Deserialize)]
struct Config {
    roles: Vec<u64>,
    #[serde(rename = "roles-alias")]
    roles_alias: Vec<String>,
    #[serde(default)]
    users: Vec<u64>,
    #[serde(rename = "url", deserialize_with = "make_calendar", serialize_with = "ser_calendar")]
    cal: (String, web_ical::Calendar),
    #[serde(default)]
    ignore: Vec<String>,
}

#[derive(Serialize, Deserialize)]
struct EdT {
    token: String,
    #[serde(deserialize_with = "serde_rwlock", serialize_with = "ser_rwlock")]
    edt: RwLock<Vec<Config>>,
}

impl EdT {
    async fn configure(&self, c: Context, msg: Message) {
        let mut args = msg.content.split(' ');
        if let Some(cmd) = args.next() {
            let mut edts = self.edt.write().await;
            match cmd {
                "nouveau" => {
                    if let Some((group, url)) = args.next().and_then(|x| args.last().map(|y| (x, y))) {
                        edts.push(Config {
                            roles: if !msg.mention_roles.is_empty() {
                                msg.mention_roles.iter().map(|x| x.0).collect()
                            } else {
                                msg.member.map(|m| m.roles.iter().map(|r| r.0).collect()).unwrap_or_default()
                            },
                            roles_alias: vec![group.to_owned()],
                            users: if !msg.mention_roles.is_empty() {
                                msg.mentions.iter().map(|x| x.id.0).collect()
                            } else {
                                vec![msg.author.id.0]
                            },
                            cal: (url.to_owned(), web_ical::Calendar::new(&url)),
                            ignore: vec![],
                        });
                    }
                },
                "ignorer" => {
                    let role = args.next().unwrap_or_default();
                    let ue = args.next().unwrap_or_default();
                    edts.iter_mut()
                        .find(|e| e.roles_alias.contains(&role.to_owned()))
                        .map(|e| e.ignore.extend(vec![ue.to_string()].into_iter()));
                },
                "ajoute" => {
                    let role = args.next().unwrap_or_default();
                    edts.iter_mut()
                        .find(|e| e.roles_alias.contains(&role.to_owned()))
                        .map(|e| {
                            e.roles.extend(msg.mention_roles.iter().map(|x| x.0));
                            e.users.extend(msg.mentions.iter().map(|x| x.id.0))
                        });
                },
                "alias" => {
                    let role = args.next().unwrap_or_default();
                    let alias = args.next();
                    if let Some(alias) = alias {
                        edts.iter_mut()
                            .find(|e| e.roles_alias.contains(&role.to_owned()))
                            .map(|e| e.roles_alias.extend(vec![alias.to_string()].into_iter()));
                    }
                },
                "suppr" | "supprimer" => {
                    let role = args.next().unwrap_or_default();
                    *edts = edts.clone().into_iter()
                        .filter(|e| !e.roles_alias.contains(&role.to_owned()))
                        .collect();
                },
                _ => return,
            }

            match std::fs::write("config.toml", toml::to_string(self).unwrap()) {
                Err(_) => { msg.channel_id.say(&c.http, "Déso, j'ai pas réussi à enregistrer.").await.ok(); }
                _ => {},
            }
        } else {
            msg.channel_id.say(&c.http, "J'ai rien compris.").await.ok();
        }
    }
}

#[serenity::async_trait]
impl EventHandler for EdT {
    async fn message(&self, c: Context, msg: Message) {
        let bot = match c.http.get_current_user().await {
            Ok(b) => b,
            _ => return,
        };

        if msg.content.starts_with("/edt ") {
            return self.configure(c, msg).await;
        }

        if msg.author.id == bot.id || !msg.mentions.iter().any(|m| m.id == bot.id) {
            return
        }

        if msg.content.ends_with(" help") {
            msg.channel_id.say(
                &c.http,
r#"Mentionne moi et je te donnes ton emploi du temps (pour aujourd'hui si les cours ne sont pas finis, sinon pour le lendemain).

Tu peux aussi préciser un groupe et/ou une date. Par exemple :

@edt-bot 13/11
@edt-bot 13/11/2019
@edt-bot IMA G7
@edt-bot 6/9 ima groupe 2
etc.

Pour me configurer, il y a la commande /edt:

/edt nouveau GROUPE URL
/edt nouveau GROUPE @Groupe URL
/edt nouveau GROUPE @Nom URL
/edt nouveau GROUPE @Groupe @Nom URL
/edt ignorer GROUPE UE
/edt ajoute GROUPE NOM
/edt alias GROUPE ALIAS
/edt suppr GROUPE"#).await.ok();
            return
        }

        let now = Utc::now().with_timezone(TZ);
        let default_date = if now.hour() < 18 {
            now
        } else {
            now + Duration::days(1)
        };
        let default_roles = msg.member.map(|m| m.roles).unwrap_or_default();

        let edt = self.edt.read().await;
        let (date, roles, ue): (_, Option<Vec<_>>, _) = msg.content
            .split(' ')
            .fold((default_date, None, None), |(d, r, ue), word| {
                match word.split('/').filter_map(|x| x.parse::<u32>().ok()).collect::<Vec<_>>().as_slice() {
                    [d, m, y] => (TZ.ymd(*y as i32, *m, *d).and_hms(12, 0, 0), r, ue),
                    [d, m] => (TZ.ymd(now.year(), *m, *d).and_hms(12, 0, 0), r, ue),
                    _ => match word.to_lowercase().as_str() {
                        "aujourd'hui" => (now, r, ue),
                        "demain" => (now + Duration::days(1), r, ue),
                        "lundi" => (next(Weekday::Mon), r, ue),
                        "mardi" => (next(Weekday::Tue), r, ue),
                        "mercredi" => (next(Weekday::Wed), r, ue),
                        "jeudi" => (next(Weekday::Thu), r, ue),
                        "vendredi" => (next(Weekday::Fri), r, ue),
                        "samedi" => (next(Weekday::Sat), r, ue),
                        "dimanche" => (next(Weekday::Sun), r, ue),
                        w => {
                            let edts: Vec<_> = edt.clone().into_iter().filter(|e| e.roles_alias.contains(&w.to_string())).collect();
                            let mut roles = r.clone().unwrap_or_default();
                            roles.extend(edts.iter().flat_map(|e| e.roles.iter().map(|i| RoleId(*i))));
                            if !edts.is_empty() {
                                (d, Some(roles), ue)
                            } else {
                                match &word[0..3] {
                                    "ang" | "his" | "rai" | "phy" | "mec" | "inf" | "mat" | "map" | "chi" | "fbi" | "etc" | "pei" => (d, r, Some(word)),
                                    _ => (d, r, ue),
                                }
                            }
                        }
                    },
                }
            });
        let roles = roles.unwrap_or(default_roles);

        let events: Vec<_> = self.edt.read().await.iter()
            .filter(|e| e.roles.iter().all(|r| roles.contains(&RoleId(*r))))
            .flat_map(|e| e.cal.clone().1.events.into_iter().filter(|evt| !e.ignore.iter().any(|i| evt.summary.contains(i))).collect::<Vec<_>>())
            .collect();
        dbg!(&events);

        if date.weekday() == Weekday::Sat || date.weekday() == Weekday::Sun {
            msg.channel_id.say(
                &c.http,
                "Wesh, c'est le week-end, pourquoi tu veux aller en cours ?"
            ).await.ok();
            return
        }

        if !events.is_empty() {
            let (days, base, events) = if let Some(ue) = ue {
                let mut events: Vec<_> = events.iter()
                    .filter(|e| e.dtstart.year() >= date.year() && e.dtstart.month() >= date.month() && e.dtstart.day() >= date.day())
                    .filter(|e| e.summary.to_lowercase().starts_with(ue))
                    .take(3)
                    .collect();

                events.sort_by_key(|e| e.dtstart);

                let text = if msg.author.id.0 == 110085098547736576 {
                    format!("Salut HuGoGoBeyBlade ! Prêt pour le combat ?\n\nTes {} prochains cours de {} sont :\n", events.len(), events.iter().next().map(|e| e.summary.clone()).unwrap_or(ue.to_string()))
                } else {
                    format!("Tes {} prochains cours de {} sont :\n", events.len(), events.iter().next().map(|e| e.summary.clone()).unwrap_or(ue.to_string()))
                };

                (true, text, events)
            } else {
                dbg!(date);
                let mut events: Vec<_> = events.iter()
                    .map(|evt| { dbg!(evt.dtstart); evt })
                    .filter(|e| e.dtstart.year() == date.year() && e.dtstart.month() == date.month() && e.dtstart.day() == date.day())
                    .collect();

                events.sort_by_key(|e| e.dtstart);

                let text = if msg.author.id.0 == 110085098547736576 {
                    format!("Bonjour HuGoGoBeyblade ! J'espère que tu as fait le plein d'hypervitesse, car voici ton emploi du temps pour le {:02}/{:02}/{:02} :\n", date.day(), date.month(), date.year())
                } else {
                    format!("Voici ton emploi du temps pour le {:02}/{:02}/{:02} :\n", date.day(), date.month(), date.year())
                };

                (false, text, events)
            };

            let response = events.iter().fold(
                base,
                |m, e| {
                    let dtstart = e.dtstart.with_timezone(TZ);
                    let dtend = e.dtend.with_timezone(TZ);

                    format!(
                        "{}— {} {}:{:02} à {}:{:02} : **{}**{}\n",
                        m,
                        if days {
                            match dtstart.weekday() {
                                Weekday::Mon => "Lundi, de",
                                Weekday::Tue => "Mardi, de",
                                Weekday::Wed => "Mercredi, de",
                                Weekday::Thu => "Jeudi, de",
                                Weekday::Fri => "Vendredi, de",
                                Weekday::Sat => "Samedi, de",
                                Weekday::Sun => "Dimanche (« manche »), de",
                            }
                        } else {
                            "De"
                        },
                        dtstart.hour(),
                        dtstart.minute(),
                        dtend.hour(),
                        dtend.minute(),
                        e.summary,
                        if !e.location.is_empty() { format!(" ({})", e.location) } else { String::new() },
                    )
                }
            );

            msg.channel_id.say(&c.http, response).await.ok();
        } else {
            msg.channel_id.say(
                &c.http,
                "Pardon, mais je n'ai pas réussi à détecter ton groupe (ou alors t'as juste pas cours)."
            ).await.ok();
        }
    }
}

#[tokio::main]
async fn main() {
    let conf: EdT = toml::from_str(&std::fs::read_to_string("config.toml").expect("Config error")).expect("de error");
    let mut client = Client::builder(conf.token.clone()).event_handler(conf).await.expect("Discord login error");

    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}
